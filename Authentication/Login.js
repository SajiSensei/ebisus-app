import React, { useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { View, TextInput, StyleSheet, Image, TouchableOpacity, Text } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView } from "react-native-safe-area-context";

//COMPONENTS
import logo from "../assets/logo.png"
import Or from '../components/or'
import Footer from '../components/footer'
import Back from '../components/back'


import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';


const Login = () => {

  {/*VARIABLES*/}
    const navigation = useNavigation();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [showInvalidAccount, setShowInvalidAccount] = useState(false);


    {/*FONTS*/}
    let [fontsLoaded, fontError] = useFonts({
      Poppins_400Regular, Poppins_900Black,
    });
  
    if (!fontsLoaded && !fontError) {
      return null;
    }  
      

    //SHOWS INVALID ACCOUNT
        const handleLogin = () => {
          console.log('Email:', email);
          console.log('Password:', password);

          setShowInvalidAccount(true);
        };
        
      
        return (
            <LinearGradient
             colors={['#31ADF3', '#81C2D700']}
            style={{ flex: 1 }}
            start={{ x: 0.5, y: 0 }}
            end={{ x: 0.5, y: 1 }}>

          {/*SCREEN CONTAINER*/}
          <SafeAreaView style={styles.container}>

          {/*BACK BUTTON*/}
          <Back/>

          {/*HEADER*/}
          <Image source={logo} style={{ width: 100, height: 100, marginTop: 30 }} />
          <Text style={[styles.header, { fontFamily: 'Poppins_400Regular' }]}>WELCOME BACK</Text>


            {/*FORM CONTAINER*/}
            <View style={styles.form}>

            {/*INPUT FIELDS*/}
            <TextInput
              style={styles.input}
              placeholder="Email"
              onChangeText={text => setEmail(text)}
              value={email}
            />
            <TextInput
              style={styles.input}
              placeholder="Password"
              onChangeText={text => setPassword(text)}
              secureTextEntry
              value={password}
            />

          {showInvalidAccount && (
          <Text style={styles.invalidAccountText}>Invalid Account</Text>
          )}

            {/*BUTTONS*/}
            <TouchableOpacity style={styles.signinbutton} onPress={() => {
              handleLogin();
            }}>
            <Text style={[styles.signintext, { fontFamily: 'Poppins_400Regular' }]}>Login</Text>
            </TouchableOpacity>

            <Or/>
            
            <TouchableOpacity style={styles.signupbutton} onPress={() => {
                navigation.navigate(
                    'CustomerRegister'
                )   
            }}>

            <Text style={[styles.signuptext, { fontFamily: 'Poppins_400Regular' }]}>Sign Up</Text>

           </TouchableOpacity>

          </View>

          </SafeAreaView>

          <Footer/>

          <StatusBar style="auto" />
          </LinearGradient>

          
        );
      };

      {/*STYLES*/}
      const styles = StyleSheet.create({
        container:{
            flex: 1,
            alignItems: 'center',
        },

        header: {
            fontWeight: 700,
            fontSize: 39,
            height: 65,
            color: '#2F3033',
            marginTop: 10,
        },

        form: {
          backgroundColor: 'white',
          borderRadius: 10,
          height: 400,
          width: 376,
          padding: 20,
          marginTop: 10,
          justifyContent: 'center',
        
        },
        input: {
          height: 40,
          borderColor: 'gray',
          borderWidth: 1,
          marginBottom: 20,
          paddingHorizontal: 10,
        },
        signinbutton: {
          width: 305,
          height: 43,
          marginTop: 25,
          marginBottom: 4,
          borderRadius: 13,
          backgroundColor: '#00A4FE',
          justifyContent: 'center',
          alignItems: 'center',
          alignSelf: 'center'
        },
        signintext: {
          fontSize: 20,
          lineHeight: 24,
          color: '#FFFFFF',
        },

        signupbutton: {
            width: 305,
            height: 43,
            marginTop: 3,
            alignSelf: 'center',
            borderRadius: 13,
            backgroundColor: '#00A4FE',
            justifyContent: 'center',
            alignItems: 'center',
          },
          signuptext: { 
            fontSize: 20,
            lineHeight: 24,
            color: '#FFFFFF',
          },

          invalidAccountText: {
            color: 'red',
            fontSize: 16,
            marginTop: 10,
            textAlign: 'center',
          },

      });


export default Login

