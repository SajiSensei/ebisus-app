import React, { useState } from 'react';
import { View, StyleSheet, TextInput, TouchableOpacity, Text } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from "react-native-safe-area-context";

//COMPONENTS
import Footer from '../components/footer'
import Back from '../components/back';


import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';
import { storeUser } from './authSlice';



const CustomerRegister = () => {

  //VARIABLES
const user = useSelector((state) => state.auth.user)
const dispatch = useDispatch()

  const navigation = useNavigation();   
  

  {/*FONTS*/}
  let [fontsLoaded, fontError] = useFonts({
    Poppins_400Regular, Poppins_900Black,
  });

  if (!fontsLoaded && !fontError) {
    return null;
  }  

        return (
            <LinearGradient
            colors={['#31ADF3', '#81C2D700']}
            style={{ flex: 1 }}
            start={{ x: 0.5, y: 0 }}
            end={{ x: 0.5, y: 1 }}>

          {/*SCREEN CONTAINER*/}
          <SafeAreaView style={styles.container}>

          {/*BACK BUTTON*/}
          <Back/>

          {/*HEADER*/}
          <Text style={[styles.header, { fontFamily: 'Poppins_400Regular' }]}>AS CUSTOMER</Text>

          {/*FORM CONTAINER*/}
            <View style={styles.form}>


              {/*NAME*/}
              <View style={styles.namecontainer}>
                <View style={styles.left}> 
                <Text>First Name</Text>
                <TextInput
                style={styles.input}
                onChangeText={(value) => dispatch(storeUser({...user, first_name: value}))}
                />
                </View>


                <View style={styles.right}>
                <Text>Last Name</Text>
                <TextInput
                style={styles.input}
                onChangeText={(value) => dispatch(storeUser({...user, last_name: value}))}
                />

                </View>
              </View>

              {/*MOBILE NUMBER*/}
              <Text>Mobile Number</Text>
                <TextInput
                style={styles.input}
                onChangeText={(value) => dispatch(storeUser({...user, mobile_number: value}))}
                />


              {/*EMAIL*/}
              <Text>Email</Text>
                <TextInput
                style={styles.input}
                onChangeText={(value) => dispatch(storeUser({...user, email: value}))}
                />
              {/*PASSWORD*/}
              <Text>Password</Text>
              <TextInput
              style={styles.input}
              secureTextEntry
              onChangeText={(value) => dispatch(storeUser({...user, password: value}))}
                />

              <Text>Confirm Password</Text>
              <TextInput
              style={styles.input}
              secureTextEntry
              onChangeText={(text) => {
              }}
              />

              <Text style={[styles.passwordtext, { fontFamily: 'Poppins_400Regular' }]}>Your password must:</Text>
              <Text style={[styles.passwordtext2, { fontFamily: 'Poppins_400Regular' }]}>be 8 to 72 characters long</Text>
              <Text style={[styles.passwordtext2, { fontFamily: 'Poppins_400Regular' }]}>not contain your name or email not be commonly used, easily guessed or contain any</Text>


              {/*AGREEMENTS*/}
              <Text style={[styles.agreements, { fontFamily: 'Poppins_400Regular' }]}>
              By continuing, you are agreeing to the
              <Text style={styles.blueText}> Terms</Text> and
              <Text style={styles.blueText}> Conditions</Text>
              </Text>


            {/*BUTTONS*/}
            <TouchableOpacity style={[styles.createacc, { fontFamily: 'Poppins_400Regular' }]} onPress={() => {
              
                console.log(user)
                navigation.navigate(
                    'Home'
                )   
            }}>

            <Text style={styles.createacctext}>Create Account</Text>

           </TouchableOpacity>

          </View>

          </SafeAreaView>

          <Footer/>
          
          </LinearGradient>

          
        );
      };
      
      {/*STYLES*/}
      const styles = StyleSheet.create({
        container:{
            flex: 1,
            alignItems: 'center',
        },

        header: {
            fontWeight: 700,
            fontSize: 39,
            height: 65,
            color: '#2F3033',
            lineHeight: 100,
        },

        form: {
          backgroundColor: 'white',
          borderRadius: 10,
          height: 620,
          width: 376,
          padding: 20,
          marginTop: 20,
        },

        namecontainer: {
          flexDirection: 'row',
          justifyContent: 'space-between',
        },
        
        left: {
          flex: 1,
          marginRight: 5,
        },
        
        right: {
          flex: 1,
          marginLeft: 5,
        },

        createacc: {
            width: 305,
            height: 43,
            marginTop: 20,
            alignSelf: 'center',
            borderRadius: 4,
            backgroundColor: '#00A4FE',
            justifyContent: 'center',
            alignItems: 'center',
            marginBottom: 20,
          },
          createacctext: {
            fontSize: 20,
            lineHeight: 24,
            color: '#FFFFFF',
          },

          input: {
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            marginTop: 5,
            paddingHorizontal: 10,
          },

          passwordtext: {
            fontSize: 10,
            lineHeight: 30,
            color: '#676D73',
          },

          passwordtext2: {
            fontSize: 10,
            lineHeight: 12.5,
            color: '#676D73',
            marginLeft: 10,
          },

          agreements:{
            fontSize: 10,
            textAlign: 'center',
            marginTop: 15,
        },

        blueText: {
          color: '#00A4FE',
          fontWeight: 'bold',
        },

      });


export default CustomerRegister