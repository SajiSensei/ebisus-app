import { createSlice } from "@reduxjs/toolkit"

export const authSlice = createSlice ({

    //INITIAL
    name: 'auth',
    initialState: {
        user: {
            first_name: '',
            last_name: '',
            mobile_number: '',
            email: '',
            password: '',
        }
    },

    //REDUCER
    reducers: {
        storeUser: (state, action) => {
            state.user = action.payload
        }
    }

})

export const { storeUser } = authSlice.actions

export default authSlice.reducer