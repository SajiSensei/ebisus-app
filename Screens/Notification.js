import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import Nav from '../components/nav';

const users = [
  // NEW
  {
    id: 1,
    name: 'Pest Assassin CDO',
    description: 'just added a discount coupon for Chinese new year!',
    time: '3h',
    image: require("../assets/idone.png"),
  },
  {
    id: 2,
    name: 'Downtown Barber',
    description: 'just entered Ebisus!',
    time: '5h',
    image: require("../assets/idtwo.png"),
  },
  // LAST 7 DAYS
  {
    id: 3,
    name: 'Uptown Barber',
    description: 'just entered Ebisus!',
    time: '5d',
    image: require("../assets/idthree.png"),
  },
  {
    id: 4,
    name: 'Laundry Max Wynn',
    description: 'just entered Ebisus!',
    time: '6d',
    image: require("../assets/idfour.png"),
  },
  {
    id: 5,
    name: 'Halord Plumbing Services',
    description: 'just entered Ebisus!',
    time: '7d',
    image: require("../assets/idfive.png"),
  },
  // LAST 30 DAYS
  {
    id: 6,
    name: 'Laundry Door',
    description: 'just entered Ebisus!',
    time: '9d',
    image: require("../assets/idfour.png"),
  },
];

const Notification = () => {
  const [notifications] = useState(users);
  const navigation = useNavigation();

  const handleIconPress = () => {
    navigation.goBack();
  };

  return (
    <View style={styles.Maincontainer} keyboardShouldPersistTaps="handled">
      <LinearGradient
        colors={['#E8F5FF', '#0095FF']}
        style={styles.header}>
        <TouchableOpacity onPress={handleIconPress} style={styles.iconContainer}>
          <Image style={styles.icon} source={require('../assets/back.png')} />
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={styles.notificationHeader}>Notifications</Text>
        </View>
      </LinearGradient>
      
      <ScrollView>

      {/* New na title */}
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionTitle}>New</Text>
        <FlatList
          style={styles.notificationList}
          enableEmptySections={true}
          data={notifications.slice(0, 2)}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.notificationBox}>
              <Image style={styles.icon} source={item.image} />
              <View style={styles.textContainer}>
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.description}>
                  <Text style={{ color: '#000' }}>{item.description}</Text> {'\u2022'} {item.time}
                </Text>
              </View>
            </View>
          )}
        />
      </View>

      {/* Last 7 Days na title */}
      <View style={styles.sectionContainer}>
        <Text style={styles.sectionTitle}>Last 7 Days</Text>
        <FlatList
          style={styles.notificationList}
          enableEmptySections={true}
          data={notifications.slice(2, 5)}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.notificationBox}>
              <Image style={styles.icon} source={item.image} />
              <View style={styles.textContainer}>
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.description}>
                  <Text style={{ color: '#000' }}>{item.description}</Text> {'\u2022'} {item.time}
                </Text>
              </View>
            </View>
          )}
        />
      </View>

      {/* Last 30 Days na title */}
      <View style={styles.last30DaysContainer}>
        <Text style={styles.sectionTitle}>Last 30 Days</Text>
        <FlatList
          style={styles.notificationList}
          enableEmptySections={true}
          data={notifications.slice(5, 6)}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item }) => (
            <View style={styles.notificationBox}>
              <Image style={styles.icon} source={item.image} />
              <View style={styles.textContainer}>
                <Text style={styles.name}>{item.name}</Text>
                <Text style={styles.description}>
                  <Text style={{ color: '#000' }}>{item.description}</Text> {'\u2022'} {item.time}
                </Text>
              </View>
            </View>
          )}
        />
      </View>

      </ScrollView>

      <Nav />
    </View>
  );
};

const styles = StyleSheet.create({
  Maincontainer: {
    backgroundColor: '#E8F5FF',
    flex: 1,
    height: '92%',
  },
  header: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    padding: 20,
    height: 120,
    overflow: 'hidden',
  },
  iconContainer: {
    left: '2%',
  },
  titleContainer: {
    flex: 1,
    marginLeft: '14%',
    marginBottom: 0,
  },
  notificationHeader: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
  },
  sectionContainer: {
    marginTop: 20,
  },
  sectionTitle: {
    fontSize: 30,
    padding: 20,
    fontWeight: 'bold',
    color: '#333',
    marginBottom: -29,
  },
  last30DaysContainer: {
    marginTop: 20,
    marginBottom: 80, 
  },
  notificationList: {
    marginTop: 10,
  },
  notificationBox: {
    padding: 20,
    marginTop: 15,
    marginLeft: 20,
    flexDirection: 'row',
    position: 'relative',
    alignItems: 'center',
  },
  icon: {
    width: 45,
    height: 45,
    marginRight: 10,
    borderRadius: 45 / 2,
  },
  textContainer: {
    flexDirection: 'column',
    flex: 1,
    marginLeft: 10,
  },
  name: {
    fontSize: 18,
    color: '#000',
    fontWeight: 'bold',
  },
  description: {
    fontSize: 16,
    color: '#888',
    marginTop: 5,
  },
});

export default Notification;