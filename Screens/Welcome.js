import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import logo from '../assets/logo.png'
import { useNavigation } from '@react-navigation/native';
import Footer from '../components/footer';
import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';
import { SafeAreaView } from "react-native-safe-area-context";

const sub = "Get started by logging in\n to your account.";


const Welcome = () => {
  
  {/*VARIABLES*/}
  const navigation = useNavigation();

  {/*FONTS*/}
  let [fontsLoaded, fontError] = useFonts({
    Poppins_400Regular, Poppins_900Black,
  });

  if (!fontsLoaded && !fontError) {
    return null;
  }  

  return (
    <>
      <LinearGradient
        colors={['#31ADF3', '#81C2D700']}
        style={{ flex: 1 }}
        start={{ x: 0.5, y: 0 }}
        end={{ x: 0.5, y: 1 }}
      >

        {/*SCREEN CONTAINER*/}
        <SafeAreaView style={styles.container}>

          {/*HEADER*/}
          <Image source={logo} style={{ width: 200, height: 200, top: 120 }} />
          <Text style={[styles.welcome, { fontFamily: 'Poppins_400Regular' }]}>WELCOME.</Text>
          <Text style={styles.sub}>{sub}</Text>


          {/*BUTTONS*/}
          <TouchableOpacity style={styles.loginbutton} onPress={() => {
                navigation.navigate(
                    'Login'
                )   
            }}>
            <Text style={styles.buttonText}>Get Started</Text>
          </TouchableOpacity>

        </SafeAreaView>

        <Footer />

        <StatusBar style="auto" />
      </LinearGradient>
    </>
  );
};

{/*STYLES*/}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },

  welcome: {
    fontWeight: 700,
    fontSize: 40,
    width: 272,
    height: 65,
    marginTop: '35%',
    color: '#2F3033',
  },

  sub: {
    fontSize: 15,
    lineHeight: 24,
    width: 181,
    height: 48,
    marginTop: '2%',
    left: -40,
  },
  
  loginbutton: {
    width: 305,
    height: 43,
    marginTop: '24%',
    borderRadius: 13,
    backgroundColor: '#00A4FE',
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonText: {
    fontSize: 20,
    lineHeight: 24,
    color: '#FFFFFF',
  },
   
});

export default Welcome;
