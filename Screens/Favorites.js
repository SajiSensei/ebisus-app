import React from 'react';
import { View, Text, StyleSheet, TextInput, Image, SafeAreaView, TouchableOpacity } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { LinearGradient } from 'expo-linear-gradient'; 
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { useNavigation } from '@react-navigation/native';

const BackButton = () => {
  const navigation = useNavigation();

  const Press = () => {
    navigation.goBack();
  };

  return (
    <TouchableOpacity onPress={Press}>
      <Image
        source={require('../assets/back.png')} 
        style={{ width: 40, height: 40, marginLeft: 20 }} 
      />
    </TouchableOpacity>
  );
};

const FavoritesScreen = () => {
  const navigation = useNavigation();

  const handleItemPress = () => {
    navigation.navigate('PestAssassinCDO');
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.searchBarContainer}>
        <TextInput
          placeholder="Search"
          style={styles.searchBar}
          textAlign="center"
        />
      </View>

      <TouchableOpacity onPress={handleItemPress}>
        <View style={styles.itemContainer}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../assets/pestassassin.png')}
              style={styles.image}
              resizeMode="cover"
            />
          </View>
          <View style={styles.descriptionContainer}>
            <Text style={styles.descriptionTitle}>Pest Assassin</Text>
            <View style={styles.textAndStarContainer}>
              <Text style={styles.descriptionText}>Pest Assassin CDO</Text>
              <View style={styles.starContainer}>
                <FontAwesome name="star" size={20} color="#FFD700" />
              </View>
            </View>
          </View>
        </View>
      </TouchableOpacity>

      <View style={styles.iconContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('Home')}>
          <Image
            source={require('../assets/search.png')}
            style={styles.smallIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Heart')}>
          <Image
            source={require('../assets/heart.png')}
            style={styles.smallIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Chats')}>
          <Image
            source={require('../assets/message.png')}
            style={styles.smallIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
          <Image
            source={require('../assets/notif.png')}
            style={styles.smallIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
          <Image
            source={require('../assets/profile.png')}
            style={styles.smallIcon}
          />
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const Stack = createStackNavigator();

const Favorites = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerBackground: () => (
          <LinearGradient
          colors={['#fff', '#0095FF']} 
          style={styles.headerBackground}
          start={{ x: 0, y: 0 }} 
          end={{ x: 0, y: 1 }} 
        />
        ),
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
          color: 'black',
          fontSize: 25,
          marginTop: 50,
        },
        headerTitleAlign: 'center',
        headerLeft: () => <BackButton />,
        headerStyle: {
          height: 120, 
        },
      }}
    >
      <Stack.Screen
        name="Favorites"
        component={FavoritesScreen}
        options={{
          title: 'Favorites',
        }}
      />
    </Stack.Navigator>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  searchBarContainer: {
    padding: 10,
    marginBottom: 10,
    marginTop: 50,
  },
  searchBar: {
    backgroundColor: '#CFEBFF',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 20,
    padding: 10,
    textAlign: 'center',
  },
  whiteView: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  text: {
    color: 'black',
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageContainer: {
    flex: 1,
    height: 100,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  descriptionContainer: {
    flex: 2,
  },
  descriptionTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  textAndStarContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    fontWeight: 'bold',
  },
  descriptionText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  starContainer: {
    alignSelf: 'flex-end',
    marginRight: 30,
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    paddingHorizontal: 1,
    paddingVertical: 10,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    borderTopWidth: 1, 
    borderTopColor: '#ccc', 
  },
  smallIcon: {
    width:32,
    height: 32,
    resizeMode: 'contain',
    margin:12,
  },
  headerBackground: {
    flex: 1,
  },
});

export default Favorites;
