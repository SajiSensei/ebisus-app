import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useFonts, Poppins_400Regular} from '@expo-google-fonts/poppins';
import Nav from '../components/nav';



const Chats = () => {
  const navigation = useNavigation();
  const [searchText, setSearchText] = useState('');

  const handleChatPress = () => {
        navigation.navigate('ChatDetails', { userId: user.id });
      };
      
  // FONT
  const [fontsLoaded] = useFonts({
    Poppins: Poppins_400Regular,
  });

  const handleIconPress = () => {
    navigation.goBack();
  };

  // PARA SA SEARCH
  const handleSearchChange = (text) => {
    setSearchText(text);
  };

  const OnlineIndicator = ({ online }) => {
    const indicatorColor = online ? 'green' : 'gray';


    return (
      <View style={[styles.onlineIndicator, { backgroundColor: indicatorColor }]} />
    );
  };

  const user = {
    id: 1,
    name: 'Pest Assassin CDO',
    description: 'yes, we are available :)',
    time: '30m',
    image: require("../assets/idone.png"),
    online: false,
  };

  
  const textStyles = {
    fontFamily: fontsLoaded ? 'Poppins' : 'Arial',  
  };

  return (
    <View style={styles.MainContainer}>
    <View style={styles.container}>
      <LinearGradient
        colors={['#E8F5FF', '#0095FF']}
        style={styles.header}>
        <TouchableOpacity onPress={handleIconPress} style={styles.iconContainer}>
          <Image style={styles.icon} source={require('../assets/back.png')} />
        </TouchableOpacity>
        <View style={styles.titleContainer}>
          <Text style={[styles.notificationHeader, textStyles]}>Chats</Text>
        </View>
      </LinearGradient>

      {/* Search Bar */}
      <View style={styles.searchContainer}>
        <TextInput
          style={[styles.searchInput, textStyles]}
          placeholder="Search"
          value={searchText}
          onChangeText={handleSearchChange}
        />
      </View>

      {/* Display chat info para sa Pest Assassin CDO */}
      <TouchableOpacity onPress={handleChatPress}>
        <View style={styles.sectionContainer}>
          <Image style={styles.icon} source={user.image} />
          <View style={styles.textContainer}>
            <Text style={[styles.name, textStyles]}>{user.name}</Text>
            <Text style={[styles.description, textStyles]}>
              <Text style={{ color: '#000' }}>{user.description}</Text> {'\u2022'} {user.time}
            </Text>
          </View>
          <OnlineIndicator online={user.online} />
        </View>
      </TouchableOpacity>
    </View>
     <Nav />

     </View>
  );
};

const styles = StyleSheet.create({
MainContainer:{
  height: '92%',
  flex: 1,
},

  container: {
    backgroundColor: '#E8F5FF',
    flex: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    padding: 20,
    height: 100,
    overflow: 'hidden',
  },
  iconContainer: {
    left: '2%',
  },
  titleContainer: {
    flex: 1,
    marginLeft: '28%',
    marginBottom: 0,
  },
  notificationHeader: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#000000',
  },
  searchContainer: {
    paddingHorizontal: 20,
    marginTop: 40,
  },
  searchInput: {
    height: 40,
    borderColor: '#C5C5C5',
    borderWidth: 1,
    borderRadius: 20,  
    paddingLeft: 10,
    backgroundColor: '#CFEBFF',
    textAlign: 'center',  
    fontWeight: 'bold', 
    fontSize: 28,
    color: '#6F6767',  
  },
  sectionContainer: {
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  icon: {
    width: 45,
    height: 45,
    borderRadius: 45 / 2,
  },
  textContainer: {
    marginLeft: 10,
    flex: 1,
  },
  name: {
    fontSize: 18,
    color: '#000',
    fontWeight: 'bold',
  },
  description: {
    fontSize: 16,
    color: '#888',
    marginTop: 5,
  },
  onlineIndicator: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginLeft: 10,
  },

  
});

export default Chats;