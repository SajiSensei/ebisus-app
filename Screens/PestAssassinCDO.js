import React from "react";
import { View,  StyleSheet, Image, ScrollView, Text } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from "react-native-safe-area-context";

//IMAGES
import ebisus from "../assets/ebisus.png";
import pestassassin from "../assets/pestassassin.png";
import pestassassin2 from "../assets/pestassassin2.jpg";
import pestassassin3 from "../assets/pestassassin3.jpg";
import pestassassinloc from "../assets/pestassassinloc.png";
import name from "../assets/name.png";
import phone from "../assets/phone.png";
import mail from "../assets/mail.png";
import star from "../assets/star.png";
import Nav from '../components/nav';


const PestAssassinCDO = () => {

    //VARIABLES
    const navigation = useNavigation();

    //FONTS
    let [fontsLoaded, fontError] = useFonts({
        Poppins_400Regular, Poppins_900Black,
      });
    
      if (!fontsLoaded && !fontError) {
        return null;
      }  

    return(

        //MAIN CONTAINER
        <SafeAreaView style={styles.maincontainer}>

            <View style={styles.container}>

                <View style={styles.headercontainer}>
                    <LinearGradient
                    colors={['#FFFFFF', '#00A5FF']}
                    style={{ flex: 1 }}
                    start={{ x: 0.5, y: 0 }}
                    end={{ x: 0.5, y: 1 }}>                   

                        <View style={styles.header}>
                        <Image source={ebisus} style={{ width: 120, height: 35, marginTop: 50, marginRight: 20  }} />
                        </View>

                    </LinearGradient>
                </View>
                
                {/*BODY CONTAINER*/}
                <ScrollView style={styles.body}>

                    {/*PORTFOLIO CONTAINER*/}
                    <ScrollView style={styles.imageportfolio}>
                    
                    {/*IMAGES*/}
                    <View style={styles.imageContainer}>

                    <Image source={pestassassin} style={styles.image} />
                    <Image source={pestassassin2} style={styles.image} />
                    <Image source={pestassassin3} style={styles.image} />
                    <Image source={pestassassinloc} style={styles.image} />

                    </View>
                    </ScrollView>

                    {/*BUSINESS NAME*/}

                    <View style={styles.business}>
                    <View style={styles.left}>
                    <Text style={[styles.businessname, { fontFamily: 'Poppins_900Black' }]}>PEST ASSASSIN</Text>
                    </View>

                    </View>
                    
                    {/*SERVICE DETAILS*/}
                    <View style={styles.details}>
                            <Text style={[styles.servicedetails, { fontFamily: 'Poppins_400Regular' }]}>Our team of professional pest exterminators in CDO use environmentally safe methods to permanently remove unwanted guests from your home or workplace.</Text>
                    </View>


                        {/*NAME*/}
                        <View style={styles.rowdetails}>
                        <Image source={name} style={styles.imagedetails} />
                        <Text style={[styles.textdetails, { fontFamily: 'Poppins_400Regular' }]}> Daren Rebote</Text>
                        </View>
                        
                        {/*PHONE NUMBER*/}
                        <View style={styles.rowdetails}>
                        <Image source={phone} style={styles.imagedetails} />
                        <Text style={[styles.textdetails, { fontFamily: 'Poppins_400Regular' }]}> 0961 019 5455</Text>
                        </View>

                        {/*MAIL*/}
                        <View style={styles.rowdetails}>
                        <Image source={mail} style={styles.imagedetails} />
                        <Text style={[styles.textdetails, { fontFamily: 'Poppins_400Regular' }]}> support@assassinsph.com</Text>
                        </View>

                        {/*REVIEWS*/}
                        <View style={styles.starreviews}>
                        <Image source={star} style={styles.star} />
                        <Image source={star} style={styles.star} />
                        <Image source={star} style={styles.star} />
                        <Image source={star} style={styles.star} />
                        <Image source={star} style={styles.star} />
                        </View>

                        {/*LINE*/}
                        <View style={styles.line} />

                        {/*COMMENT REVIEWS*/}
                        <Text style={[styles.reviews, { fontFamily: 'Poppins_400Regular' }]}>REVIEWS</Text>
                        <Text style={[styles.reviewtext, { fontFamily: 'Poppins_400Regular' }]}>Be the first to review</Text>


                </ScrollView>

            </View>



            <Nav/>

        </SafeAreaView>

    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        backgroundColor: 'white',
    },

    container: {
        height: '92%',
    },

    headercontainer: {
        height: '13%',
        top: '-5%',
        borderRadius: 51,
        overflow: 'hidden',
    },

    header:{
        alignItems: 'flex-end',
    },

    imageportfolio: {
        padding: 15,
        borderRadius: 1,
        borderWidth: 6,
        borderColor: '#DDD5D5',
        height: 220,
        width: '90%',
        alignSelf: 'center',
    },
    
    imageContainer: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    image: {
        width: '95%',
        height: 150,
        marginTop: 10,
        marginBottom: 10,
    },
    
    business:{
        flexDirection: 'row',   
        marginTop: '4%',
        marginLeft: '2%',
        marginBottom: '2%',
        },

    left:{
        width: '75%',
    },

    right:{
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },

    businessname:{
        fontSize: 23,
        marginLeft: '5%',
    },

    servicedetails:{
        alignSelf: 'center',
        maxWidth: '80%',
        marginLeft: '2%',
        fontWeight: 400,
        fontSize: 12,
        lineHeight: 20,
    },

    rowdetails:{
        flexDirection: 'row',
        marginLeft: '5%',
        marginTop: '5%',
    },

    imagedetails:{
        height: 20,
        width: 20,
    },

    textdetails:{
        fontSize: 12,
    },

    starreviews: {
        justifyContent: 'center',
        flexDirection: 'row',
        marginTop: 20,
    },

    star:{
        height: 20,
        width: 20,
        margin: 10,
    },

    line: {
        height: 1,
        backgroundColor: 'gray',
        margin: 20,
    },

    reviews:{
        fontSize: 20,
        lineHeight: 20,
        marginLeft: 40,
    },

    reviewtext:{
        fontSize: 12,
        lineHeight: 20,
        textDecorationLine: 'underline',
        marginLeft: 40,
        marginTop: 20,
    },

});

export default PestAssassinCDO;