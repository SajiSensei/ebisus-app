import React from "react";
import { StyleSheet, View, Image, Text, TextInput, ScrollView, TouchableOpacity } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { SafeAreaView } from "react-native-safe-area-context";
import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';
import Nav from '../components/nav';


//IMAGES
import ebisus from "../assets/ebisus.png";
import location from "../assets/location.png";
import pest from "../assets/pest.png";


const Home = () => {

    //VARIABLES
    // hi its me manager
    const navigation = useNavigation();

    //FONTS
    let [fontsLoaded, fontError] = useFonts({
        Poppins_400Regular, Poppins_900Black,
      });
    
      if (!fontsLoaded && !fontError) {
        return null;
      }  

    return (
        
        
        //MAIN CONTAINER
        <SafeAreaView style={styles.maincontainer}>

            <View style={styles.container}> 
            {/*HEADER CONTAINER*/}
            <View style={styles.headercontainer}>
            <LinearGradient
             colors={['#FFFFFF', '#00A5FF']}
            style={{ flex: 1 }}
            start={{ x: 0.5, y: 0 }}
            end={{ x: 0.5, y: 1 }}>
            
            {/*LOCATION*/}
            <View style={styles.location}>
            <Text style={[styles.textlocation, { fontFamily: 'Poppins_400Regular' }]}>
            <Image source={location} style={{ width: 10, height: 10 }} />
             {' '}Cagayan de Oro City </Text>
                
            </View>
             
            {/*HEADER*/}
            <View style={styles.header}>
            <Image source={ebisus} style={{ width: 200, height: 80, marginTop: 40 }} />

            {/* Search Bar */}
            <TextInput
            style={styles.searchBar}
            placeholder="Search..."
            placeholderTextColor="#000000"
            />

        
            </View>
            </LinearGradient>
            </View>


            {/*BODY*/}
            <ScrollView  style={styles.body}>
            <View>

            {/* ROW 1 */}
            <View style={styles.row}>
                {/* PEST CONTROL */}
                <TouchableOpacity
                style={styles.border}
                onPress={() => navigation.navigate('PestControl')}
                >
                <Image source={pest} style={styles.imageservices} />
                <Text style={styles.textservices}>PEST CONTROL</Text>
                </TouchableOpacity>
            </View>

            </View>
            </ScrollView>
            </View>

            {/* NAVIGATION ICONS */}
            <Nav/>
            
        </SafeAreaView>

    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        backgroundColor: 'white',
    },

    container: {
        height: '92%',
    },
    

    headercontainer: {
        height: '38%',
        top: '-5%',
        borderRadius: 51,
        overflow: 'hidden',
    },

    header:{
        alignItems: 'center',
        justifyContent: 'center',
    },

    textlocation:{
        fontSize: 12,
        
        marginTop: 50,
        marginLeft: '2%',
    },

    searchBar: {
        width: '80%',
        height: 40,
        backgroundColor: '#FFFFFF',
        borderRadius: 47,
        marginTop: 30,
        paddingLeft: 20,
    },

    
    body:{
        top: '-4%',
    },

    row:{
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
        marginLeft: '7%',
    },

    imageservices:{
        height: 50,
        width: 50,
    },

    textservices:{
        fontSize: 18,
        marginTop: 10,
    },

    border: {
        borderWidth: 1,
        borderColor: '#B0B0B0',
        borderRadius: 5,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        height: 140,
        width: 170,
      },


});

export default Home;
