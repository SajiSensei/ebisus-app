import React, { useState } from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, Image, TextInput } from "react-native";
import { LinearGradient } from 'expo-linear-gradient';
import { useFonts, Poppins_400Regular, Poppins_900Black } from '@expo-google-fonts/poppins';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from "react-redux";
import { storeUser } from "../Authentication/authSlice";
import { SafeAreaView } from "react-native-safe-area-context";

//COMPONENTS


//IMAGES
import edit from "../assets/edit.png"
import back from '../assets/back.png'
import check from '../assets/check.png'

import Nav from '../components/nav';


const Profile = () => {

    //VARIABLES
    const navigation = useNavigation();
    const dispatch = useDispatch();
    const user = useSelector((state) => state.auth.user);
    

    const [editing, setEditing] = useState(false);
    const [editedFirstName, setEditedFirstName] = useState(user.first_name);
    const [editedLastName, setEditedLastName] = useState(user.last_name);
    const [editedEmail, setEditedEmail] = useState(user.email);
    const [editedPassword, setEditedPassword] = useState(user.password);
    const [editedMobileNumber, setEditedMobileNumber] = useState(user.mobile_number);


    const handleEditPress = () => {
        setEditing(true);
    };

    const handleSavePress = () => {

        // Dispatch action to update Redux state
        dispatch(storeUser({
            ...user,
            first_name: editedFirstName,
            last_name: editedLastName,
            email: editedEmail,
            password: editedPassword,
            mobile_number: editedMobileNumber
        }));

        // Exit editing mode
        setEditing(false);
    };

    
    //FONTS
    let [fontsLoaded, fontError] = useFonts({
        Poppins_400Regular, Poppins_900Black,
      });
    
      if (!fontsLoaded && !fontError) {
        return null;
      }  

    return(

        //MAIN CONTAINER
        <SafeAreaView style={styles.maincontainer}>

            <View style={styles.container}>
                

                {/*HEADER*/}
                <View style={styles.headercontainer}>
                    <LinearGradient
                    colors={['#E8F5FF', '#0095FF']}
                    style={{ flex: 1 }}
                    start={{ x: 0.5, y: 0 }}
                    end={{ x: 0.5, y: 1 }}>      

                    {/*BACK BUTTON*/}
                    <View style={styles.rowheader}> 
                        <TouchableOpacity style={styles.backbutton} onPress={() => {
                        navigation.goBack(
                        )   
                        }}>
                        <Image source={back} style={{ width: 50, height: 50, marginLeft: 15}} />
                        </TouchableOpacity>
                                

                            <View style={styles.header}>
                            <Text style={[styles.headertext, { fontFamily: 'Poppins_400Regular' }]}>Profile</Text>
                            </View>

                    </View>

                    </LinearGradient>
                
                
                </View>

                
                {/*BODY CONTAINER*/}

                <ScrollView style={styles.body}>

                {/*ACCOUNT EDIT*/}
                    
                <View style={styles.editview}>
                {editing ? (
                        <TouchableOpacity style={styles.edit} onPress={handleSavePress}>
                            <Image source={check} style={{ height: 20, width: 20, marginBottom: 5 }} />
                        </TouchableOpacity>
                            ) : (
                            <TouchableOpacity style={styles.edit} onPress={handleEditPress}>
                                <Image source={edit} style={{ height: 20, width: 20, marginBottom: 5 }} />
                            </TouchableOpacity>
                            )}

                </View>

                {/*FIRST NAME*/}
                <View style={styles.infobox}>
                    <View style={styles.infoboxhead}>

                        <View style={styles.left}>
                            <Text style={[styles.infotext, { fontFamily: 'Poppins_400Regular' }]}>First Name</Text>
                        </View>

                        <View style={styles.right}>
                            
                        </View>

                    </View>

                    <View style={styles.infodetails}>
                            {editing ? (
                                <TextInput
                                    style={styles.textinput}
                                    value={editedFirstName}
                                    onChangeText={setEditedFirstName}
                                />
                            ) : (
                                <Text style={styles.textdetails}> {user.first_name}  </Text>
                            )}
                    </View>
                

                </View>

                {/*LAST NAME*/}
                <View style={styles.infobox}>
                    <View style={styles.infoboxhead}>

                        <View style={styles.left}>
                            <Text style={[styles.infotext, { fontFamily: 'Poppins_400Regular' }]}>Last Name</Text>
                        </View>

                        <View style={styles.right}>
                            
                        </View>

                    </View>

                    <View style={styles.infodetails}>
                            {editing ? (
                                <TextInput
                                    style={styles.textinput}
                                    value={editedLastName}
                                    onChangeText={setEditedLastName}
                                />
                            ) : (
                                <Text style={styles.textdetails}> {user.last_name}  </Text>
                            )}
                    </View>
                

                </View>


                {/*EMAIL*/}
                <View style={styles.infobox}>
                    <View style={styles.infoboxhead}>

                        <View style={styles.left}>
                            <Text style={[styles.infotext, { fontFamily: 'Poppins_400Regular' }]}>Email</Text>
                        </View>

                        <View style={styles.right}>
                            
                        </View>

                    </View>

                    <View style={styles.infodetails}>
                            {editing ? (
                                <TextInput
                                    style={styles.textinput}
                                    value={editedEmail}
                                    onChangeText={setEditedEmail}
                                />
                            ) : (
                                <Text style={styles.textdetails}> {user.email}  </Text>
                            )}
                    </View>
                

                </View>

                {/*PASSWORD*/}
                <View style={styles.infobox}>
                    <View style={styles.infoboxhead}>

                        <View style={styles.left}>
                            <Text style={[styles.infotext, { fontFamily: 'Poppins_400Regular' }]}>Password</Text>
                        </View>

                        <View style={styles.right}>
                            
                        </View>

                    </View>

                    <View style={styles.infodetails}>
                            {editing ? (
                                <TextInput
                                    style={styles.textinput}
                                    value={editedPassword}
                                    onChangeText={setEditedPassword}
                                />
                            ) : (
                                <Text style={styles.textdetails}> {Array(editedPassword.length + 1).join('*')} </Text>
                            )}
                    </View>
                

                </View>


                {/*MOBILE NUMBER*/}
                <View style={styles.infobox}>
                    <View style={styles.infoboxhead}>

                        <View style={styles.left}>
                            <Text style={[styles.infotext, { fontFamily: 'Poppins_400Regular' }]}>Mobile Number</Text>
                        </View>

                        <View style={styles.right}>
                            
                        </View>

                    </View>

                    <View style={styles.infodetails}>
                            {editing ? (
                                <TextInput
                                    style={styles.textinput}
                                    value={editedMobileNumber}
                                    onChangeText={setEditedMobileNumber}
                                />
                            ) : (
                                <Text style={styles.textdetails}> {user.mobile_number} </Text>
                            )}
                    </View>
                

                </View>
            

                {/*LINE*/}
                <View style={styles.line} />

                </ScrollView>


               

            </View>



            <Nav/>

        </SafeAreaView>

    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    maincontainer: {
        flex: 1,
        backgroundColor: '#E8F5FF',
    },

    container: {
        height: '92%',
    },

    headercontainer: {
        height: '18%',
        top: '-5%',
        overflow: 'hidden',
    },
    
    header: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    rowheader:{
        flexDirection:'row',
        marginTop: '15%',
    },

    headertext:{
        fontSize: 25,
        marginTop: '8%',
        marginRight: '13%',
        fontWeight: 700,
        color: 'black',
    },

    body: {
        top: '-4%',
    },

    infobox:{
        height: 90,
        width: 340,
        backgroundColor: 'white',
        alignSelf: 'center',
        borderRadius: 12,
        marginTop: '7%',
        shadowColor: '#000',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 0.25,
        shadowRadius: 4,
    },

    infoboxhead:{
        flexDirection: 'row',
    },

    infotext:{
        fontSize: 13,
        marginLeft: '7%',
        marginTop: '5%',
    },

    left:{
        width: '75%',
    },

    right:{
        width: '25%',
        alignItems: 'flex-end',
        justifyContent: 'center',
    },

    editview: {
        flex: 1,
        alignItems: 'flex-end',
    },

    edit: {
        marginTop: '3%',
        marginRight: '6%',
    },

    line: {
        height: 1,
        backgroundColor: '#C5C5C5',
        margin: 20,
    },

      textdetails: {
        fontSize: 15,
        fontWeight: 'bold',
        marginLeft: '5%',
        marginTop: '2%',
      },

      textinput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        borderRadius: 8,
        margin: 10,
        paddingHorizontal: 10,
    },

    border: {
        borderColor: 'blue',
    },

});

export default Profile;