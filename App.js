import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import store from './Authentication/store'
import { Provider } from 'react-redux';
import { StatusBar } from 'expo-status-bar';

//SCREENS
import Welcome from './Screens/Welcome'
import Login from './Authentication/Login'
import CustomerRegister from './Authentication/CustomerRegister'
import Home from './Screens/Home'
import Notification from './Screens/Notification'
import Favorites from './Screens/Favorites'
import PestControl from './Screens/PestControl'
import PestAssassinCDO from './Screens/PestAssassinCDO'
import Profile from './Screens/Profile'
import Chats from './Screens/Chats';

//VARIABLES
const Stack = createStackNavigator();

const Mystack = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
      <StatusBar backgroundColor="black" />
        <Stack.Navigator initialRouteName="Welcome">
          <Stack.Screen name="Welcome" component={Welcome} options={{headerShown: false}} />
          <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
          <Stack.Screen name="CustomerRegister" component={CustomerRegister} options={{headerShown: false}} />
          <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
          <Stack.Screen name="PestAssassinCDO" component={PestAssassinCDO} options={{headerShown: false}} />
          <Stack.Screen name="Notification" component={Notification} options={{headerShown: false}} />
          <Stack.Screen name="Favorites" component={Favorites} options={{headerShown: false}} />
          <Stack.Screen name="PestControl" component={PestControl} options={{headerShown: false}} />
          <Stack.Screen name="Profile" component={Profile} options={{headerShown: false}} />
          <Stack.Screen name="Chats" component={Chats} options={{headerShown: false}} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default Mystack;