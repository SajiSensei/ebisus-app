import React from "react";
import {  StyleSheet, Image, View, TouchableOpacity } from "react-native";
import { useNavigation } from '@react-navigation/native';

//IMAGES
import search from "../assets/search.png";
import heart from "../assets/heart.png";
import message from "../assets/message.png";
import notif from "../assets/notif.png";
import profile from "../assets/profile.png";



const Nav = () => {

    //VARIABLES
    const navigation = useNavigation();

    return (
        <View style={styles.nav}>
            <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Image source={search} style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Favorites')}>
            <Image source={heart} style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Chats')}>
            <Image source={message} style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
            <Image source={notif} style={styles.icon} />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate('Profile')}>
            <Image source={profile} style={styles.icon} />
            </TouchableOpacity>
        </View>
    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    nav: {
        position: 'fixed',
        flexDirection: 'row',
        justifyContent: 'space-around', 
        backgroundColor: 'white',
        bottom: 0,
        height: '8%',
        width: '100%',
        alignItems: 'center',
        borderTopWidth: 1,
        borderColor: '#B0B0B0',
    },

    icon: {
        width: 30,
        height: 30,
    },

});

export default Nav;