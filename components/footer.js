import React from "react";
import { Text, StyleSheet, Image, View } from "react-native";
import ebisus from '../assets/ebisus.png';

const Footer = () => {
    return (
        <View style={styles.footer}>
        <Text style={styles.footerText}> <Image source={ebisus} style={{ width: 60, height: 20, top: 4 }} /> ⓒ 2023 EBISUS, Inc.</Text>
        </View>
    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    footerText:{
        fontSize: 10,
        color: 'gray',
        lineHeight: 30,
    
      },
});

export default Footer;
