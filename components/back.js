import React from "react";
import { StyleSheet, Image, TouchableOpacity } from "react-native";
import back from '../assets/back.png';
import { useNavigation } from '@react-navigation/native';

const Back = () => {

    const navigation = useNavigation();

    return (
        <TouchableOpacity style={styles.backbutton}onPress={() => {
            navigation.goBack(
                )   
                }}>
        <Image source={back} style={{ width: 50, height: 50, marginLeft: 15}} />
        </TouchableOpacity>
    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    backbutton:{
        right: '43%',
        top: '2%',
    },
});

export default Back;
