import React from "react";
import { Text, StyleSheet, View } from "react-native";


const Or = () => {
    return (
        <View style={styles.orContainer}>
        <View style={styles.line} />
        <Text style={styles.orText}>or</Text>
        <View style={styles.line} />
        </View> 
    );
};

{/*STYLES*/}
const styles = StyleSheet.create({
    orContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    line: {
        height: 1,
        backgroundColor: 'gray',
        flex: 1,
    },
    orText: {
        color: 'gray',
        marginHorizontal: 10,
    },
});

export default Or;
